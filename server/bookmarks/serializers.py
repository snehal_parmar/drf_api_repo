from rest_framework import serializers
from bookmarks.models import Bookmark

class BookmarkSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Bookmark model """
    class Meta:
        model = Bookmark
        fields = ("id", "name", "description", "website")
