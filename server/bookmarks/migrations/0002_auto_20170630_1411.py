# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookmarks', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookmark',
            name='founded_date',
        ),
        migrations.AddField(
            model_name='bookmark',
            name='added_date',
            field=models.CharField(default=b'2017-06-30 14:11:44.882166', max_length=500),
        ),
        migrations.AddField(
            model_name='bookmark',
            name='modified_date',
            field=models.CharField(default=b'2017-06-30 14:11:44.882000', max_length=500),
        ),
    ]
