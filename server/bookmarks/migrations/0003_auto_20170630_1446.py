# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookmarks', '0002_auto_20170630_1411'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookmark',
            name='added_date',
            field=models.CharField(default=b'2017-06-30 14:46:13.598448', max_length=500),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='description',
            field=models.CharField(default=b'', max_length=1000),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='modified_date',
            field=models.CharField(default=b'2017-06-30 14:46:13.598208', max_length=500),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='website',
            field=models.URLField(default=b'', max_length=500),
        ),
    ]
