# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookmarks', '0003_auto_20170630_1446'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookmark',
            name='added_date',
            field=models.CharField(default=b'2017-07-14 19:28:29.993871', max_length=500),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='id',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
        migrations.AlterField(
            model_name='bookmark',
            name='modified_date',
            field=models.CharField(default=b'2017-07-14 19:28:29.993647', max_length=500),
        ),
    ]
