from django.db import models
import datetime

# Create your models here.
class Bookmark(models.Model):
    """ Bookmark  model"""
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, default="")
    modified_date = models.CharField(max_length=500, default=str(datetime.datetime.utcnow()))
    added_date = models.CharField(max_length=500, default=str(datetime.datetime.utcnow()))
    website = models.URLField(max_length=500, default="")
