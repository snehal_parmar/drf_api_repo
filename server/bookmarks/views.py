# from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from bookmarks.models import Bookmark
from bookmarks.serializers import BookmarkSerializer

class BookmarkViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Chain objects """
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer