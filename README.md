# README #

This is sample app with backbone frontend and django restful framework 


### What is this repository for? ###

* Sample code for backbone 
* 0.0.1


### How do I get set up? ###

#Please install the requirement using the following the command:
pip install -r requirement.txt

#Running the server using following:
python server/manage.py runserver 8002

#Open the backbone frontend page in broswer:
client/index_bookmarks.html

#If Cors error comes please use the following plugin in chrome broser:
Allow-Control-Allow-Origin: * 
https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi
